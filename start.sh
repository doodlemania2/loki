#!/bin/bash

set -eu

echo "=> sending config"

if [ ! -f "/app/data/loki-config.yaml" ]; then
	cp /app/code/loki-config.yaml /app/data/loki-config.yaml
fi

if [ ! -f "/app/data/.configured" ]; then
	touch /app/data/.configured
fi
echo "=> starting loki"
exec "/app/code/loki-linux-amd64" -config.file=/app/data/loki-config.yaml
