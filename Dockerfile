FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

WORKDIR /app/code
RUN mkdir -p /app/code && cd /app/code && \
    curl -O -L "https://github.com/grafana/loki/releases/download/v2.0.0/loki-linux-amd64.zip" && \
    unzip "loki-linux-amd64.zip" && \
    chmod a+x "loki-linux-amd64" && \
    curl -O -L "https://github.com/grafana/loki/releases/download/v2.0.0/logcli-linux-amd64.zip" && \
    unzip "logcli-linux-amd64.zip" && \
    chmod a+x "logcli-linux-amd64" && \
    curl -O -L "https://github.com/grafana/loki/releases/download/v2.0.0/loki-canary-linux-amd64.zip" && \
    unzip "loki-canary-linux-amd64.zip" && \
    chmod a+x "loki-canary-linux-amd64"
EXPOSE 3100
ADD --chown=cloudron:cloudron start.sh loki-config.yaml /app/code/
RUN mkdir -p /app/data && chown -R cloudron:cloudron /app/data && chmod +x /app/code/start.sh
ENTRYPOINT [ "/app/code/start.sh" ]
